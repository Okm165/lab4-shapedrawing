#pragma once

#include "shape.h"

namespace Shapes {
    class Rectangle : public Shape {
    public:
        Rectangle(int xFrom, int yFrom, int xTo, int yTo) : m_xFrom(xFrom), m_yFrom(yFrom), m_xTo(xTo), m_yTo(yTo) {}
        bool isIn(int x, int y) const {
            return (x >= m_xFrom && x <= m_xTo && y >= m_yFrom && y <= m_yTo);
        }

        int x() const {
            return m_xFrom;
        }

        int y() const {
            return m_yFrom;
        }

        int xTo() const {
            return m_xTo;
        }

        int yTo() const {
            return m_yTo;
        }
    private:
        int m_xFrom, m_yFrom, m_xTo, m_yTo;
    };
}