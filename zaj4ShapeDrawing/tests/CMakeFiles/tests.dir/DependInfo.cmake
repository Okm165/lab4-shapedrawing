
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/bartosz/Documents/agh/cpp/lab4-shapedrawing/zaj4ShapeDrawing/circle.cpp" "tests/CMakeFiles/tests.dir/__/circle.cpp.o" "gcc" "tests/CMakeFiles/tests.dir/__/circle.cpp.o.d"
  "/home/bartosz/Documents/agh/cpp/lab4-shapedrawing/zaj4ShapeDrawing/rectangle.cpp" "tests/CMakeFiles/tests.dir/__/rectangle.cpp.o" "gcc" "tests/CMakeFiles/tests.dir/__/rectangle.cpp.o.d"
  "/home/bartosz/Documents/agh/cpp/lab4-shapedrawing/zaj4ShapeDrawing/tests/shapesTests.cpp" "tests/CMakeFiles/tests.dir/shapesTests.cpp.o" "gcc" "tests/CMakeFiles/tests.dir/shapesTests.cpp.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
