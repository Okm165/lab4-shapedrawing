#pragma once

#include "shape.h"

#define CIRCLE_H_EXISTS

namespace Shapes {
    class Circle : public Shape {
    public:
        Circle(int xCenter, int yCenter, int radius) : m_xCenter(xCenter), m_yCenter(yCenter), m_radius(radius) {}
        bool isIn(int x, int y) const {
            int dx = x - m_xCenter;
            int dy = y - m_yCenter;
            return (dx * dx + dy * dy <= m_radius * m_radius);
        }

        int x() const {
            return m_xCenter;
        }

        int y() const {
            return m_yCenter;
        }

        int getRadius() const {
            return m_radius;
        }
    private:
        int m_xCenter, m_yCenter, m_radius;
    };
}
